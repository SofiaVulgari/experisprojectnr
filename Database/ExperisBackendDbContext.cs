﻿using ExperisProjectNR.Model;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace ExperisProjectNR.Database
{
    public class ExperisBackendDbContext : DbContext
    {
        /// <summary>
        /// Database tables
        /// </summary>
        
        public DbSet<Group>? Groups => Set<Group>();
        public DbSet<User>? Users => Set<User>();

        public ExperisBackendDbContext([NotNull] DbContextOptions options) : base(options) { }

        /// <summary>
        /// Seed some initial data
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            SeedData.Seed(modelBuilder);
        }
    }

}
