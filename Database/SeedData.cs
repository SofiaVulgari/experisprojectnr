﻿using ExperisProjectNR.Model;
using Microsoft.EntityFrameworkCore;

namespace ExperisProjectNR.Database
{
    public class SeedData
    {
        public static void Seed(ModelBuilder modelBuilder)
        {

            //Create Users
            User Sofia = new()
            {
                Id = 1,
                FirstName = "Sofia",
                LastName = "Vulgari",
                Username = "SoVu"
            };

            User Elias = new()
            {
                Id = 2,
                FirstName = "Elias",
                LastName = "Haloun",
                Username = "ElHa"
            };

            User Mattias = new()
            {
                Id = 4,
                FirstName = "Mattias",
                LastName = "Eriksson",
                Username = "MaEk"
            };

            #region Groups

            // Create Groups
            Group Växjö = new()
            {
                Id = 1,
                Name = "Växjö Group",
                Description = "All experis employees in Växjö",
                GroupPublicity = "Public"
            };

            Group FDevelopers = new()
            {
                Id = 2,
                Name = "Frontend Developers",
                Description = "All frontend developers of experis",
                GroupPublicity = "Public"
            };

            #endregion

           

           

           
            // Seed Groups
            modelBuilder.Entity<Group>().HasData(Växjö);
            modelBuilder.Entity<Group>().HasData(FDevelopers);

           
        }
    }
}
