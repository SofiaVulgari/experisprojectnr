﻿using ExperisProjectNR.Model;
using ExperisProjectNR.Model.DTOs.Group;
using Microsoft.AspNetCore.Mvc;

namespace ExperisProjectNR.Services.Interfaces
{
    /// <summary>
    /// Service Interface for the Group
    /// </summary>
    public interface IGroupService
    {
        public Task<ActionResult<IEnumerable<Group>>> GetAllGroups();
        public Task<ActionResult<Group>> GetGroupById(int id);
        public Task<Group> CreateGroup(Group group);
        public Task<bool> UpdateGroup(Group group);
        public Task<Group> DeleteGroup(int id);
        public bool GroupEntityExists();
        public Task<bool> GroupExists(int id);
        //public Task<ActionResult<IEnumerable<User>>> GetUsersInGroup(int id);
        //public Task<bool> UpdateUsersInGroup(int id, List<int> users);
    }
}
