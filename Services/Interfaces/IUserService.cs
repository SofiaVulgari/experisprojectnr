﻿using ExperisProjectNR.Model.DTOs.User;
using ExperisProjectNR.Model;
using Microsoft.AspNetCore.Mvc;

namespace ExperisProjectNR.Services.Interfaces
{
    /// <summary>
    /// Service Interface for the User
    /// </summary>
    public interface IUserService
    {
        public Task<ActionResult<IEnumerable<User>>> GetAllUsers();
        public Task<ActionResult<User>> GetUserById(int id);
        public Task<User> CreateUser(User user);
        public Task<bool> UpdateUser(int id, User user);
        public Task<User> DeleteUser(int id);
        public bool UserEntityExists();
        public Task<bool> UserExists(int id);
    }
}
