﻿using ExperisProjectNR.Database;
using ExperisProjectNR.Model;
using ExperisProjectNR.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ExperisProjectNR.Services
{
    /// <summary>
    /// Group Service
    /// </summary>
    public class GroupService : IGroupService
    {
        private readonly ExperisBackendDbContext _context;

        public GroupService(ExperisBackendDbContext context)
        {
            _context = context;
        }

        public async Task<Group> CreateGroup(Group group)
        {
            await _context.Groups.AddAsync(group);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return group;
        }

        public async Task<Group> DeleteGroup(int id)
        {
            var group = await _context.Groups.FindAsync(id);

            if (group != null)
            {
                _context.Groups.Remove(group);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }

            return group;
        }

        public async Task<ActionResult<IEnumerable<Group>>> GetAllGroups()
        {
            return await _context.Groups.ToListAsync();
        }

        public async Task<ActionResult<Group>> GetGroupById(int id)
        {
            if (_context.Groups == null)
                throw new Exception("There are no groups in the database");

            return await _context.Groups
                .Where(f => f.Id == id).FirstAsync();
        }

        public bool GroupEntityExists()
        {
            if (_context.Groups == null)
                return false;

            return true;
        }

        public async Task<bool> GroupExists(int id)
        {
            return await _context.Groups.AnyAsync(m => m.Id == id);
        }

        public async Task<bool> UpdateGroup(Group group)
        {
            _context.Entry(group).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return true;
        }

        //public async Task<ActionResult<IEnumerable<User>>> GetUsersInGroup(int id)
        //{
        //    return await _context.Groups.Where(p => p.Id == id).Select(p => p.Users).FirstOrDefaultAsync();
        //}

        //public async Task<bool> UpdateUsersInGroup(int id, List<int> users)
        //{
        //    List<User> domainMembers = new();
        //    foreach (var user in users)
        //    {
        //        domainMembers.Add(await _userRepository.GetUserById(user));
        //    }

        //    return await _groupRepository.UpdateUsersInGroup(id, domainMembers);
        //}

    }
}
