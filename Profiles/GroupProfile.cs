﻿using AutoMapper;
using ExperisProjectNR.Model;
using ExperisProjectNR.Model.DTOs.Group;

namespace ExperisProjectNR.Profiles
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
            CreateMap<Group, GroupReadDTO>().ForMember(s => s.Users, opt => opt
                .MapFrom(src => src.Users.Select(a => a.Id).ToArray())).ReverseMap();

            CreateMap<Group, GroupCreateDTO>().ReverseMap();

            CreateMap<Group, GroupUpdateDTO>().ReverseMap();
        }
    }
}
