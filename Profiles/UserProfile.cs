﻿using ExperisProjectNR.Model;
using AutoMapper;
using ExperisProjectNR.Model.DTOs.User;

namespace ExperisProjectNR.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserReadDTO>()
                .ForMember(u => u.Groups, opt => opt
                .MapFrom(src => src.Groups.Select(g => g.Id).ToArray()))
                .ReverseMap();

            CreateMap<User, UserCreateDTO>().ReverseMap();

            CreateMap<User, UserUpdateDTO>().ReverseMap();
        }
    }
}
