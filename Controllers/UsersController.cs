﻿using AutoMapper;
using ExperisProjectNR.Model.DTOs.User;
using ExperisProjectNR.Model;
using ExperisProjectNR.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using ExperisProjectNR.Database;
using Microsoft.EntityFrameworkCore;
using ExperisProjectNR.Model.DTOs.Group;
using ExperisProjectNR.Services;

namespace ExperisProjectNR.Controllers
{
    /// <summary>
    /// Users Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly ExperisBackendDbContext _context;

        public UsersController(IUserService userService, IMapper mapper, ExperisBackendDbContext context)
        {
            _userService = userService;
            _mapper = mapper;
            _context = context;
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns>All the groups</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetAllUsers()
        {
            return _mapper.Map<List<UserReadDTO>>(await _userService.GetAllUsers());
        }

        /// <summary>
        /// Get specific user by id
        /// </summary>
        /// <returns>All the groups</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<UserReadDTO>> GetUserById(int id)
        {
            return _mapper.Map<UserReadDTO>(await _userService.GetUserById(id));
        }

        /// <summary>
        /// Update a user by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userUpdateDTO"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(int id, UserUpdateDTO userUpdateDTO)
        {
            if (id != userUpdateDTO.Id)
            {
                return BadRequest();
            }

            if (!await _userService.UserExists(id))
                return NotFound();

            var group = _userService.GetUserById(id);
            _mapper.Map(userUpdateDTO, group);
            _context.Entry(group).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserEntityExists())
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }


            return NoContent();
        }

        /// <summary>
        /// Create a user
        /// </summary>
        /// <param name="userCreateDTO"></param>
        /// <returns>The created user</returns>
        [HttpPost]
        public async Task<ActionResult<User>> CreateUser(UserCreateDTO userCreateDTO)
        {
            if (!UserEntityExists())
                return Problem("Entity is null");

            var user = _mapper.Map<User>(userCreateDTO);
            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            //var group = await _groupService.CreateGroup(groupCreateDTO);

            return CreatedAtAction("Create User", new { id = user.Id }, _mapper.Map<UserCreateDTO>(user));
        }

        /// <summary>
        /// Delete a user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            if (!UserEntityExists())
                return Problem("Entity is null.");

            var user = await _userService.DeleteUser(id);

            if (user == null)
                return NotFound();

            return NoContent();
        }

        private bool UserEntityExists()
        {
            return _userService.UserEntityExists();
        }
    }
}
