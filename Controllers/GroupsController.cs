﻿using AutoMapper;
using ExperisProjectNR.Model.DTOs.Group;
using ExperisProjectNR.Model;
using ExperisProjectNR.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using ExperisProjectNR.Database;
using Microsoft.EntityFrameworkCore;

namespace ExperisProjectNR.Controllers
{
    /// <summary>
    /// Groups Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class GroupsController : ControllerBase
    {
        private readonly IGroupService _groupService;
        private readonly IMapper _mapper;
        private readonly ExperisBackendDbContext _context;

        /// <summary>
        /// Group service
        /// </summary>
        /// <param name="groupService"></param>
        public GroupsController(IGroupService groupService, IMapper mapper, ExperisBackendDbContext context)
        {
            _groupService = groupService;
            _mapper = mapper;
            _context = context;
        }

        /// <summary>
        /// Get all groups
        /// </summary>
        /// <returns>All the groups</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetAllGroups()
        {
           // return _mapper.Map<List<GroupReadDTO>>(await _groupService.GetAllGroups());
            if (_context.Groups == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<GroupReadDTO>>(await _context.Groups.Include(f => f.Users).ToListAsync());
        }

        /// <summary>
        /// Get specific group by id
        /// </summary>
        /// <returns>All the groups</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<GroupReadDTO>> GetGroupById(int id)
        {
            if (_context.Groups == null)
            {
                return NotFound();
            }
            var group = await _context.Groups.Include(m => m.Users).Where(m => m.Id == id).FirstOrDefaultAsync();

            if (group == null)
            {
                return NotFound();
            }

            return _mapper.Map<GroupReadDTO>(group);
        }

        /// <summary>
        /// Update a group by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="groupUpdateDTO"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateGroup(int id, GroupUpdateDTO groupUpdateDTO )
        {
            if (id != groupUpdateDTO.Id)
            {
                return BadRequest();
            }

            if (!await _groupService.GroupExists(id))
                return NotFound();

            var group = _groupService.GetGroupById(id);
            _mapper.Map(groupUpdateDTO, group);
            _context.Entry(group).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GroupEntityExists())
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }


            return NoContent();
        }

        /// <summary>
        /// Create a group
        /// </summary>
        /// <param name="groupCreateDTO"></param>
        /// <returns>The created group</returns>
        [HttpPost]
        public async Task<ActionResult<GroupReadDTO>> CreateGroup(GroupCreateDTO groupCreateDTO)
        {
            if (!GroupEntityExists())
                return Problem("Entity is null");

            var group = _mapper.Map<Group>(groupCreateDTO);
            _context.Groups.Add(group);
            await _context.SaveChangesAsync();

            //var group = await _groupService.CreateGroup(groupCreateDTO);

            return CreatedAtAction("Create Group", new { id = group.Id }, _mapper.Map<GroupCreateDTO>(group));
        }

        /// <summary>
        /// Delete a group by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGroup(int id)
        {
            if (!GroupEntityExists())
                return Problem("Entity is null.");

            var group = await _groupService.DeleteGroup(id);

            if (group == null)
                return NotFound();

            return NoContent();
        }

        private bool GroupEntityExists()
        {
            return _groupService.GroupEntityExists();
        }

        /// <summary>
        /// Get all users of a group by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>All members of the specified group</returns>
        //[HttpGet("{id}/users")]
        //public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetAllUsersInGroup(int id)
        //{
        //    if (await _groupService.GetGroupById(id) == null)
        //        return BadRequest();

        //    return await _groupService.GetUsersInGroup(id);
        //}

        ///// <summary>
        ///// Updates a member(user) in a group
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="users"></param>
        ///// <returns>The created group</returns>
        //[HttpPut("users/{id}")]
        //public async Task<ActionResult> UpdateUsersInGroup(int id, List<int> users)
        //{
        //    var updated = await _groupService.UpdateUsersInGroup(id, users);
        //    if (updated) return NoContent();

        //    return BadRequest();
        //}
    }
}
