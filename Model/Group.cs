﻿using System.ComponentModel.DataAnnotations;

namespace ExperisProjectNR.Model
{
    public class Group
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string? Name { get; set; }
        public string? Description { get; set; }
        [MaxLength(10)]
        public string? GroupPublicity { get; set; } //Private or Public
        public int? ManagerId { get; set; }
        public ICollection<User>? Users { get; set; }
        public float? X { get; set; }
        public float? Y { get; set; }
        public float? Z { get; set; }
        
    }
}
