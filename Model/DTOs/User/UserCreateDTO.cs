﻿namespace ExperisProjectNR.Model.DTOs.User
{
    /// <summary>
    /// A  data transfer object for the creation of a user.
    /// Protects from potential exposure of other sensitive information.
    /// </summary>
    public class UserCreateDTO
    {
        public string? Role { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Username { get; set; }
        public string? Password { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public bool? IsFirstAccess { get; set; } // Change Password
        public bool? IsActive { get; set; }
        public bool? IsTwo_Factor_Enabled { get; set; }
        public ICollection<int>? Groups { get; set; }
    }
}
