﻿namespace ExperisProjectNR.Model.DTOs.Group
{
    /// <summary>
    /// A  data transfer object for the creation of a group.
    /// Protects from potential exposure of other sensitive information.
    /// </summary>
    public class GroupCreateDTO
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? GroupPublicity { get; set; }
        public int? ManagerId { get; set; }
        public float? X { get; set; }
        public float? Y { get; set; }
        public float? Z { get; set; }
    }
}
