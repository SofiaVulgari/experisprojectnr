﻿using System.ComponentModel.DataAnnotations;

namespace ExperisProjectNR.Model
{
    public class User
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string? Role { get; set; }
        [MaxLength(100)]
        public string? FirstName { get; set; }
        [MaxLength(100)]
        public string? LastName { get; set; }
        [MaxLength(20)]
        public string? Username { get; set; }
        [MaxLength(6)]
        public string? Password { get; set; }
        [MaxLength(100)]
        public string? Email { get; set; }
        [MaxLength(20)]
        public string? Phone { get; set; }
        public bool? IsFirstAccess { get; set; } // Change Password
        public bool? IsActive { get; set; }
        public bool? IsTwo_Factor_Enabled { get; set; }
        public ICollection<Group>? Groups { get; set; }
    }
}
